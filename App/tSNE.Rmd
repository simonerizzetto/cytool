---
title: "t-SNE cytof"
output: 
  pdf_document:
    fig_width: 20
    fig_height: 24 
params:
  df: NA
  legend: NA
---

```{r echo=FALSE, results='hide',message=FALSE}
df<-params$df

  p1<-ggplot(df) + geom_point(data = df, aes(x = tsne...1., y = tsne...2., colour=factor(`Group (healthy=A, ACT=B, No ACT=C)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Group (healthy=A, ACT=B, No ACT=C)")
  p2<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=`This sample "early" or "late"`) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="This sample \"early\" or \"late\"")
  p5<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=as.numeric(Age)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Age") 
  p6<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(Gender)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Gender") 
  p7<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`CMV (R/D)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="CMV (R/D)") 
  p8<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`CMV risk`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="CMV risk") 
  p9<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(Diagnosis)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Diagnosis") 
  p10<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`Conditioning`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Conditioning") 
  p11<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`T cell depletion`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="T cell depletion")
  p12<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`Donor (Sib, MMRel, MUD, Haplo, cord)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Donor (Sib, MMRel, MUD, Haplo, cord)")
  p13<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`This sample pre or post CMV reactivation?`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="This sample pre or post CMV reactivation?")
  p14<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`CMV reactivation (Y=1/N=0)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="CMV reactivation (Y=1/N=0)")
  p26<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=as.numeric(`DPT of CMV reactivation`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="DPT of CMV reactivation")
  p15<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=as.numeric(`DPT of last CMV pos in blood`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="DPT of last CMV pos in blood")
  p16<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=as.numeric(`Duration of CMV`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Duration of CMV")
  p17<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`CD8 Immune profile seen at any time`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="CD8 Immune profile seen at any time")
  p18<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`CMV Disease?`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="CMV Disease?") 
  p19<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`aGVHD Grade 2 to 4(Y/N)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="aGVHD Grade 2 to 4(Y/N)")
  p20<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=as.numeric(`DPT of aGVHD`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="DPT of aGVHD")
  p21<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`This sample pre or post GVHD?`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="This sample pre or post GVHD?")
  p22<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`Overall aGVHD grade`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Overall aGVHD grade") 
  p23<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`cGVHD (Y/N)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="cGVHD (Y/N)")
  p24<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`Relapse post transplant (Y/N)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Relapse post transplant (Y/N)")
  p25<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`Alive/Dead`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Alive/Dead") 

if (!params$legend) {
  
  
  
  p1<-ggplot(df) + geom_point(data = df, aes(x = tsne...1., y = tsne...2., colour=factor(`Group (healthy=A, ACT=B, No ACT=C)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Group (healthy=A, ACT=B, No ACT=C)") + theme(legend.position="none")
  p2<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=`This sample "early" or "late"`) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="This sample \"early\" or \"late\"") + theme(legend.position="none")
  p5<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=as.numeric(Age)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Age")  + theme(legend.position="none")
  p6<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(Gender)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Gender")  + theme(legend.position="none")
  p7<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`CMV (R/D)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="CMV (R/D)")  + theme(legend.position="none")
  p8<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`CMV risk`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="CMV risk")  + theme(legend.position="none")
  p9<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(Diagnosis)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Diagnosis")  + theme(legend.position="none")
  p10<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`Conditioning`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Conditioning")  + theme(legend.position="none")
  p11<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`T cell depletion`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="T cell depletion") + theme(legend.position="none")
  p12<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`Donor (Sib, MMRel, MUD, Haplo, cord)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Donor (Sib, MMRel, MUD, Haplo, cord)") + theme(legend.position="none")
  p13<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`This sample pre or post CMV reactivation?`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="This sample pre or post CMV reactivation?") + theme(legend.position="none")
  p14<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`CMV reactivation (Y=1/N=0)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="CMV reactivation (Y=1/N=0)") + theme(legend.position="none")
  p26<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=as.numeric(`DPT of CMV reactivation`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="DPT of CMV reactivation") + theme(legend.position="none")
  p15<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=as.numeric(`DPT of last CMV pos in blood`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="DPT of last CMV pos in blood") + theme(legend.position="none")
  p16<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=as.numeric(`Duration of CMV`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Duration of CMV") + theme(legend.position="none")
  p17<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`CD8 Immune profile seen at any time`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="CD8 Immune profile seen at any time") + theme(legend.position="none")
  p18<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`CMV Disease?`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="CMV Disease?")  + theme(legend.position="none")
  p19<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`aGVHD Grade 2 to 4(Y/N)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="aGVHD Grade 2 to 4(Y/N)") + theme(legend.position="none")
  p20<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=as.numeric(`DPT of aGVHD`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="DPT of aGVHD") + theme(legend.position="none")
  p21<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`This sample pre or post GVHD?`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="This sample pre or post GVHD?") + theme(legend.position="none")
  p22<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`Overall aGVHD grade`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Overall aGVHD grade")  + theme(legend.position="none")
  p23<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`cGVHD (Y/N)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="cGVHD (Y/N)") + theme(legend.position="none")
  p24<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`Relapse post transplant (Y/N)`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Relapse post transplant (Y/N)") + theme(legend.position="none")
  p25<-ggplot(df) + geom_point(aes(x = tsne...1., y = tsne...2., colour=factor(`Alive/Dead`)) ) + labs(x = "t-SNE 1", y="t-SNE 2", colour="Alive/Dead")  + theme(legend.position="none")

  
  
  
}

```


```{r} 
multiplot(p1,p2,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p26,p15,p16,p17,p18,p19,p20,p21,p22,p23,p24,p25, cols=3)

```
