# README #

This README contains the steps necessary to get this application up and running.

### What is this repository for? ###

* This is a Shiny App that allows analysis and interpretation of Cytof data
* Version 1.0

### How do I get set up? ###

* This app is based on R and Shiny plus a list of R packages listed in the setup.R file
* First you need to install R. Rstudio is also recommended to easly run and install this app.
* Run the setup.R file to install all R packages required.
* Run the app.R file to execute the Shiny app.

### Features ###

Check the wiki of this bitbcuket here https://bitbucket.org/simonerizzetto/cytool/wiki/Home for a list of all functionalities

### Troubleshooting ###

* The installation of some R packages could fail due to missing libraries in you OS. To solve this issue we suggest to post the error to one of the many R communities online (e.g. https://stackoverflow.com/questions/tagged/r)
* For any issue you can contact us at s.rizzetto at unsw.edu.au